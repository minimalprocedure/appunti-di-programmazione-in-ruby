module Memozation
  def print_cahe
    puts @@lt
  end

  def memoize(mname)
    @@lt ||= Hash.new { |h, k| h[k] = {} }

    fun = Module.instance_method(mname)
    p fun
    define_method(mname) { |*args|
      @@lt[mname][args] = fun.bind(self).call(*args) unless @@lt[mname].include?(args)
      @@lt[mname][args]
    }
  end
end

require 'benchmark'
include Benchmark

include Memozation

high_limit = 39

memoize def fib(x)
          x < 2 ? x : fib(x - 1) + fib(x - 2)
        end

memoize def fib_rec(x)
  def f(x, acc, piv)
    x == 0 ? acc : f(x - 1, piv, acc + piv)
  end
  f(x, 0, 1)
end

def fib_range(r)
  r.map { |n| fib(n) }
end

def fib_range_rec(r)
  r.map { |n| fib_rec(n) }
end

Benchmark.benchmark(CAPTION, 7, FORMAT, ">total:", ">avg:") do |x|
  rep1 = x.report(:fib_range){ fib_range(0..high_limit) }
  rep2 = x.report(:fib_range_rec){ fib_range_rec(0..high_limit) }
  [rep1 + rep2, (rep1+rep2)/2]
end

