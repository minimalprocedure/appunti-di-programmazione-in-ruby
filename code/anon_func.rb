@lmbd1 = lambda {return "sono una lambda 1"}
@lmbd2 = ->{return "sono una lambda 2"}
@proc = Proc.new {return "sono una proc"}

def call_lmbd
  puts "calling lambda..."
  puts @lmbd1.call
  puts @lmbd2.call
  puts "continuo..."
end

def call_proc
  puts "calling proc..."
  puts @proc.call
  puts "continuo..."
end

call_lmbd
call_proc
