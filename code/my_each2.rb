def my_each(coll)
  for e in coll do
    yield e
  end if block_given?
end

my_each(1..5) { |n|
  puts "questo loop ciclerà #{n} volte"
}
