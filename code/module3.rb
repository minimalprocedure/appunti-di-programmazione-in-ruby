CONSTANT = "sono una costante sulla radice"

module MyModule
  CONSTANT = "sono una costante"

  def c
    p CONSTANT
  end

  module MyModuleNested
    def c_from_parent
      p CONSTANT
    end

    def c_from_root
      p ::CONSTANT
    end
  end
end

class MyClass
  include MyModule
  include MyModule::MyModuleNested
end

k = MyClass.new

k.c_from_parent
k.c_from_root
