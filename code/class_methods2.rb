class Parent
  class << self
    def attr_acc(s)
      define_method("#{s}=") do |v|
        instance_variable_set("@#{s}", v)
      end
      define_method("#{s}") do
        instance_variable_get("@#{s}")
      end
    end
  end
end

class Child < Parent
  attr_acc :value

  def sum(a)
    a + @value
  end
end

c1 = Child.new
c1.value = 10
p c1.sum(10)
