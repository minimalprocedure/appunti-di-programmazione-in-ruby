class Pippo

  def method_missing(name, *args, &block)
    puts "method: #{name} arguments: #{args.join(',')}"
  end

end

pippo = Pippo.new

pippo.m(1, 2, 3)
