module A
  $str_a = "pippo"
  $sym_a = :pippo
end

puts $str_a.object_id

module B
  $str_a = "pippo2"
  $str_b = "pippo"
  $sym_b = :pippo
end

puts $str_a.object_id
puts $str_b.object_id
puts $sym_a.object_id
puts $sym_b.object_id
