nomi = %w(Paola Giorgio Andrea)
saluti = %w(buongiorno buonasera buonanotte)
titoli = %w(Dott. Sig. Caro)
lettera = <<~HD
Le scrivo mio malgrado per comunicarle che...

Distinti saluti

Dr. Mario Rossi
HD

lettere = titoli.zip(nomi, saluti).map { |e|
  [[e.take(2), "#{e.last}."].flatten.join(' '), lettera]
}

lettere.each_with_index { |l, i|
  name = "lettera-#{i}.txt"
  puts "salvo il file #{name}"
  File.open(name, 'w') { |f| f << l.join("\n\n") }
}
