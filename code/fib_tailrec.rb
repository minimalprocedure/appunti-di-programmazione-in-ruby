require 'benchmark'
include Benchmark

high_limit = 39

def fib(x)
  def fun(x, nxt, cur)
    x == 0 ? cur : fun(x - 1, cur + nxt, nxt)
  end
  fun(x, 1, 0)
end

def fib_range(r)
  r.map { |n| fib(n) }
end

Benchmark.benchmark(CAPTION, 7, FORMAT, ">total:", ">avg:") { |x|
  rep = x.report(:fib_range){ fib_range(1..high_limit) }
  [rep, rep]
}
