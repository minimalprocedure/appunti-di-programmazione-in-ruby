def my_each(coll, &block)
  for e in coll do
    block.call(e)
  end
end

my_each(1..5) { |n|
  puts "questo loop ciclerà #{n} volte"
}
