somma = ->(a, b) { a + b } #Nuova sintassi per le lambda

def applica_la_funzione_alla_lista(f, lista)
  lista.reduce(&f)
end

puts applica_la_funzione_alla_lista(somma, [1, 2, 3, 4, 5]) #=> 15
