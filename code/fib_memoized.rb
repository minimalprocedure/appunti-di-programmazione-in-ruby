require 'benchmark'
include Benchmark

require './memoization'
include Memoization

high_limit = 20000

memoize(
  def fib(x)
    x < 2 ? x : fib(x - 1) + fib(x - 2)
  end
)

def fib_range(r)
  r.map { |n| fib(n) }
end

Benchmark.benchmark(CAPTION, 7, FORMAT, ">total:", ">avg:") do |x|
  rep = x.report(:fib_range){ fib_range(0..high_limit) }
  [rep, rep]
end
