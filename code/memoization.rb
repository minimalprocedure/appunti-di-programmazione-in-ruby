module Memoization
  def memoize(mname)
    @@lt ||= Hash.new { |h, k| h[k] = {} }
    fun = Module.instance_method(mname)
    define_method(mname) { |*args|
      @@lt[mname][args] = fun.bind(self).call(*args) unless @@lt[mname].include?(args)
      @@lt[mname][args]
    }
  end
end
