require 'sinatra'

get '/hello/:who' do |who|
  "Hello #{who}"
end
