class Parent
  class << self
    def attr_acc(s)
      define_method("#{s}=") do |v|
        instance_variable_set("@#{s}", v)
      end
      define_method("#{s}") do
        instance_variable_get("@#{s}")
      end
    end
  end
end

class Child2 < Parent
  def sum(a)
    a + @value
  end
end
Child2.attr_acc(:value)

c2 = Child2.new
c2.value = 25
p c2.sum(10)
