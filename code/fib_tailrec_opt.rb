require 'benchmark'
include Benchmark

high_limit = 39 #10100

def fib_range_recomp(r)
  RubyVM::InstructionSequence.compile_option = {
    tailcall_optimization: true,
    trace_instruction: false
  }
  RubyVM::InstructionSequence.new(<<-EOS).eval
    def fib(x)
      def fun(x, nxt, cur)
        x == 0 ? cur : fun(x - 1, cur + nxt, nxt)
      end
      fun(x, 1, 0)
    end
    def fib_range(r)
      r.map { |n| fib(n) }
    end
    fib_range(#{r})
    EOS
end

Benchmark.benchmark(CAPTION, 7, FORMAT, "> total:", "> avg:") { |x|
  rep = x.report(:fib_range_recomp){ fib_range_recomp(0..high_limit) }
  [rep, rep]
}
