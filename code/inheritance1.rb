class Parent
  def age
    "l'età di Parent è 56"
  end
end

class Child < Parent

  alias :parent_age :age

  def age
    "l'età di Child è 13"
  end
end

c = Child.new

p c.age
p c.parent_age
