require 'sinatra'

def current_user
  {name: "Mario", lastname: "Rossi", role: :any}
end

set(:auth) do |*roles|
  condition do
    if roles.include?(current_user[:role])
      true
    else
      redirect "login", 303
    end
  end
end

get "/admin", :auth => [:admin] do
  "Amministrazione"
end

get "/area", :auth => [:any] do
  "Area pubblica"
end

get "/login" do
  "LOGIN"
end
