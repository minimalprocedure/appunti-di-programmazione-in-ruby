class Alfabeto
  def initialize
    @text = []
  end

  def text
    @text.join
  end

  #https://unicode-table.com/en/search/?q=heart
  # 💕 = U+1F495
  (('a'..'z').to_a << '_' << '💕').each { |l|
    define_method(l.to_sym) {
      @text << (l == '_' ? " " : l)
      self
    }
  }
end

alf = Alfabeto.new
p alf.m.a.r.i.o._.r.o.s.s.i._.💕.text
