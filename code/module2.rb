module MyModule
  def met(v)
    p v
  end
  module MyModuleNested
    def met2(v)
      p "nested module: #{v}"
    end
  end
end

class MyClass
  include MyModule
  include MyModule::MyModuleNested
end

k = MyClass.new

k.met("metodo dal modulo")
k.met2("metodo dal modulo")
