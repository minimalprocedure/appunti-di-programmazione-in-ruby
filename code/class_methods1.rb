class Parent
  def met_public
    "sono pubblico"
  end

  def met_private
    "sono privato"
  end

  def met_protected
    "sono protetto"
  end

  protected :met_protected
  private :met_private
end

class Child < Parent
  def call_protected
    self.met_protected
  end

  def call_private
    self.met_private
  end
end

c = Child.new
puts c.met_public
puts c.call_protected
puts c.call_private
