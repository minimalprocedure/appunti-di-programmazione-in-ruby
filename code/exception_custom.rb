class PermissionDeniedError < StandardError

  attr_reader :action

  def initialize(message, action)
    super(message)
    @action = action
  end
end

begin
  raise PermissionDeniedError.new("Permesso negato", :delete)
rescue PermissionDeniedError => e
  puts "Exception Class:     #{ e.class.name }"
  puts "Exception Message:   #{ e.message }"
  puts "Exception Action:   #{ e.action }"
  puts "Exception Backtrace: #{ e.backtrace }"
end
