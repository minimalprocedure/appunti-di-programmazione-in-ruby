TIPI_ALIMENTO = {
  "pere": "frutta",
  "mele": "frutta",
  "succo": "bibita",
  "manzo": "carne",
  "sarago": "pesce",
  "mozzarella": "latticino"
}

def trova_per_tipo(t)
  TIPI_ALIMENTO.reduce([]) {|memo, c| memo << c[0].to_s if c[1] == t; memo}
end

p trova_per_tipo("frutta")
p trova_per_tipo("pesce")
