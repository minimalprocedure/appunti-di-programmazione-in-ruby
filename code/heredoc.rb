s1 = <<HD
  - Il terminale deve essere allineato. -
  sono una stringa
  lunga
HD

s2 = <<-HD
  - Il terminale può essere indentato. -
  sono una stringa
  lunga
  HD

s3 = <<~HD
  - Il terminale può essere indentato e la stringa viene allineata. -
  sono una stringa
  lunga
  HD

puts s1
puts s2
puts s3
