module MyModule
  def met(v)
    p v
  end
end

class MyClass
  include MyModule
end

k = MyClass.new

k.met("metodo dal modulo")
