TIPI_ALIMENTO = {
  "pere": "frutta",
  "mele": "frutta",
  "succo": "bibita",
  "manzo": "carne",
  "sarago": "pesce",
  "mozzarella": "latticino"
}

class AccArray < Array
  def initialize(t)
    @t = t
  end

  def get(tuple)
    self << tuple[0].to_s if tuple[1] == @t
    self
  end
end

def trova_per_tipo(t)
  TIPI_ALIMENTO.reduce(AccArray.new(t), :get)
end

p trova_per_tipo("frutta")
p trova_per_tipo("pesce")
