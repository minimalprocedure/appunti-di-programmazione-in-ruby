PERSON = {
  name: "Mario",
  lastname: "Rossi"
}

class HashAccessor

  def initialize(data)
    @data = data
  end

  define_method(:name) { @data[:name] }
  define_method(:"name=") { |v| @data[:name] = v }
  define_method(:lastname) { @data[:lastname] }
  define_method(:"lastname=") { |v| @data[:lastname] = v }

end

person = HashAccessor.new(PERSON)

puts person.name
person.name = "Maria"
puts person.name
puts person.lastname
