require 'sinatra'
require './helpers/main_helpers'
helpers MainHelpers

set :server, %w[thin]
set :port, 3000
set :bind, '0.0.0.0'

get "/" do
  slim :index, :layout => :"layouts/layout", :locals => {year: 2018, author: "Braccobaldo"}
end

get "/stylesheets/style.css", :provides => :css do
  scss :"scss/style"
end
