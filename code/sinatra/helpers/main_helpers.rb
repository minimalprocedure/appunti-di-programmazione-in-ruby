module MainHelpers

  def link_to(text, href, **options)
    %(<a href=#{href} title=#{options[:title].to_s} class=#{options[:class]}>#{text}</a>)
  end

  def image(name, **options)
    %(<img src="/images/#{name}" alt="#{options[:alt].to_s} />)
  end
end
