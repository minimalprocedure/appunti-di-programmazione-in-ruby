PERSON = {
  name: "Mario",
  lastname: "Rossi"
}

class HashAccessor

  def initialize(data)
    @data = data
    prepareClass
  end

  def prepareClass
    @data.each { |key, value|
      self.class.send(:define_method, key.to_s) { @data[key] }
      self.class.send(:define_method, "#{key.to_s}=") { |v| @data[key] = v }
    }
  end

end

person = HashAccessor.new(PERSON)

puts person.name
person.name = "Maria"
puts person.name
