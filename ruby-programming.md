Interprete
--

+ Installazione
+ linea di comando
+ Interactive Ruby Shell (IRB) ed alternative
+ variabili d'ambiente

Linguaggio
--

+ parole chiave
+ tipi di dati
+ letterali
+ variabili
+ costanti, costanti predefinite
+ operatori
+ metodi
+ strutture di controllo e flusso
+ programmazione orientata agli oggetti
	* classi
	* moduli
	* ereditarietà
	* mixin
+ programmazione funzionale
	* funzioni
	* lambda
	* Proc
	* closure
	* map/reduce
+ meta-programmazione

Libreria
--

+ libreria base (core library)
+ libreria standard (standard library)

Strumenti
--

+ editor con supporto Ruby
+ rvm (Ruby Version Manager)
+ gemme ruby e RubyGems
+ bundler

Web Framework
--

+ macro frameworks (Ruby On Rails)
+ micro frameworks (Sinatra)
